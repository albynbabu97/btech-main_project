# btech-final_year_project

Enhancing Vehicle Control Using A Combined Signboard Approach

/backup : contains the final weight file at iteration no. 3000

/cfg : contains the congiguraton file of model 'yolov3.cfg'

/data : 
> /img : contains the images and labelled txt file

> /labels : contains the labels for displaying text and box

> /valid : contains images to validate (multiple image per time)

> obj.data : details on train, valid and locations of backup file and class names

> obj.names : contains list of classes

> train.txt : list of images included in training set
    
/examples : contains c files for training

/include : darknet header file

/obj : object files

/src : sourse files

Makefile : file for making

darknet : made file

# Steps

make the file in the inside root folder

